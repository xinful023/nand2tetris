#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fu
"""

import sys,re,os

file_in = sys.argv[-1]
f = open(file_in,'r+')

file = "" # output file content
r = 1
for line in f:
    if sys.argv[1] == "no-comments":
        if re.match(r'\s*/\*',line):
            r = 0
        if line.endswith('*/\n'):
            r = 1
            continue
        if r == 1:
            line = re.sub(r'//.*$','',line) # use regular expression to remove substring starting with //
        else:
            continue
        
    line = re.sub(r'\s+',' ',line) # use regular expression to remove white space
    if line != " ":
        file += line[:-1] + '\n' # add new line
        
file_out = os.path.splitext(file_in)[0] + '.out'
a = open(file_out,'w')
a.write(file[:-1]) # remove the last new line

f.close()