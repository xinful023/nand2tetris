#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fu
"""

import os

class VMWriter:
    def __init__(self,filename):
        self.vm_filename = os.path.splitext(filename)[0] + '.vm'
        self.vm = open(self.vm_filename,'w')
        
        self.segment_names = ['constant','argument','local','static','this','that','pointer','temp']
        self.command_names = ['add','sub','neg','eq','gt','lt','and','or','not']
        
        self.symboltablekindtovmwritersegment = {'static':'static','field':'this','argument':'argument','var':'local'}

    def writePush(self,segment,index):
        if(segment in self.symboltablekindtovmwritersegment):
            segment = self.symboltablekindtovmwritersegment[segment]
        if(isinstance(segment,str)):
            segment.lower()
        self.vm.write('push ' + segment + ' ' + str(index) + '\n')
    
    def writePop(self,segment,index):
        if(segment in self.symboltablekindtovmwritersegment):
            segment = self.symboltablekindtovmwritersegment[segment]
        if(isinstance(segment,str)):
            segment.lower()
        self.vm.write('pop ' + segment + ' ' + str(index) + '\n')
    
    def writeArithmetic(self,command):
        if(isinstance(command,str)):
            command.lower()
        self.vm.write(command + '\n')
    
    def writeLabel(self,label):
        self.vm.write('label ' + label + '\n')
    
    def writeGoTo(self,label):
        self.vm.write('goto ' + label + '\n')
    
    def writeIf(self,label):
        self.vm.write('if-goto ' + label + '\n')
    
    def writeCall(self,name,nArgs):
        self.vm.write('call ' + name + ' ' + str(nArgs) + '\n')
    
    def writeFunction(self,name,nLocals):
        self.vm.write('function ' + name + ' ' + str(nLocals) + '\n')
    
    def writeReturn(self):
        self.vm.write('return' + '\n')
    
    def close(self):
        self.vm.close()