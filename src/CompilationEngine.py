#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fu
"""

import os

from VMWriter import VMWriter

class CompilationEngine:
    def __init__(self,jt):
        self.jt = jt
        self.xml_filename = os.path.splitext(jt.jack_filename)[0] + '.xml'
        self.xml = open(self.xml_filename,'w')
                
        self.tabk = 0
        self.symbol_table = SymbolTable()
        self.identifier_category = ''
        self.identifier_type = ''
        self.current_class = ''
        self.current_subroutine = ''
        self.subroutine_locals = 0
        self.subroutine_args = 0
        self.void_return = False;
        
        self.vm_writer = VMWriter(jt.jack_filename)
        
        self.statement_label_tag = 0
        
        if self.jt.getTokenValue() == 'class':
            self.compileClass()
        
    def __del__(self):
        self.xml.close()
        
    def setIdentifierCategory(self,icategory):
        self.identifier_category = icategory
        
    def setIdentifierType(self,itype):
        self.identifier_type = itype
        
    def setCurrentClass(self):
        self.current_class = self.jt.getTokenValue()
        
    def setCurrentSubroutine(self):
        self.current_subroutine = self.jt.getTokenValue()
        
    def writeXmlTag(self,s,tag_type):
        if tag_type == 'begin':
            self.xml.write('\t'*self.tabk + '<' + s + '>\n')
            self.tabk += 1
        if tag_type == 'end':
            self.tabk -= 1
            self.xml.write('\t'*self.tabk + '</' + s + '>\n')
            
    def writeXmlTerminal(self,tokenType = None,tokenValue = None):
        if not tokenType:
            tokenType = self.jt.xml_translate[self.jt.getTokenType()]
        if not tokenValue:
            tokenValue = str(self.jt.getTokenValue())
            
        if tokenValue == '<':
            tokenValue = '&lt;'
        if tokenValue == '>':
            tokenValue = '&gt;'
        if tokenValue == '&':
            tokenValue = '&amp;'
        
        self.xml.write('\t'*self.tabk + '<' + tokenType + '> ' + tokenValue + ' </' + tokenType + '>\n')
        
    def processTerminal(self):
        self.writeXmlTerminal()
        if self.jt.getTokenType() == 'IDENTIFIER':
            self.updateSymbolTable()
            
    def updateSymbolTable(self,token_value = None):
        if not token_value:
            token_value = self.jt.getTokenValue()
            
        if self.identifier_category in ('var','argument'):
            if token_value not in self.symbol_table.subroutine_table:
                self.symbol_table.define(token_value,self.identifier_type,self.identifier_category)
        elif self.identifier_category in ('static','field'):
            if token_value not in self.symbol_table.class_table:
                self.symbol_table.define(token_value,self.identifier_type,self.identifier_category)
                        
    def compileClass(self):
        self.writeXmlTag('class','begin')
        self.processTerminal()
        self.jt.nxt()
        
        self.setCurrentClass()
        self.setIdentifierCategory('class')
        self.processTerminal()
        self.jt.nxt()
        
        self.processTerminal()
        self.jt.nxt()
        
        while self.jt.getTokenValue() != '}':
            if self.jt.getTokenValue() in ('static','field'):
                self.compileVarDec('class')
            if self.jt.getTokenValue() in ('constructor','function','method'):
                self.compileSubroutine()
        self.processTerminal()
        self.jt.nxt()
        
        self.writeXmlTag('class','end')
        
    def compileVarDec(self,varScope):            
        if varScope == 'class':
            self.writeXmlTag('classVarDec','begin')
        else:
            self.writeXmlTag('varDec','begin')
        category = self.jt.getTokenValue()
        self.processTerminal()
        self.jt.nxt()
        
        if self.jt.getTokenType() != 'KEYWORD':
            self.setIdentifierCategory('class')
            self.setIdentifierType(self.jt.getTokenValue())
        self.processTerminal()
        self.jt.nxt()
        
        self.setIdentifierCategory(category)
        self.processTerminal()
        self.jt.nxt()
        
        if varScope == 'subroutine':
            self.subroutine_locals += 1;
        while(self.jt.getTokenValue() != ';'):
            self.processTerminal()
            self.jt.nxt()
            
            self.processTerminal()
            self.jt.nxt()
            
            if varScope == 'subroutine':
                self.subroutine_locals += 1;
        self.processTerminal()
        self.jt.nxt()
        
        if varScope == 'class':
            self.writeXmlTag('classVarDec','end')
        else:
            self.writeXmlTag('varDec','end')
            
    def compileSubroutine(self):
        self.writeXmlTag('subroutineDec','begin')
        self.symbol_table.startSubroutine()
        self.subroutine_locals = 0
        self.statement_label_tag = 0
        self.void_return = False;
        subroutineType = self.jt.getTokenValue()
        self.processTerminal()
        self.jt.nxt()
        
        if self.jt.getTokenValue() == 'void':
            self.void_return = True;
        self.setIdentifierCategory('class')
        self.setIdentifierType(self.jt.getTokenValue())
        self.processTerminal()
        self.jt.nxt()
        
        self.setIdentifierCategory('subroutine')
        self.processTerminal()
        self.setCurrentSubroutine()
        self.jt.nxt()
        
        if subroutineType == 'method':
            self.symbol_table.define('this',self.current_class,'argument')
        self.processTerminal()
        self.jt.nxt()
        
        self.compileParameterList()
        self.processTerminal()
        self.jt.nxt()
        
        self.writeXmlTag('subroutineBody','begin')
        self.processTerminal()
        self.jt.nxt()
        
        while self.jt.getTokenValue() == 'var':
            self.compileVarDec('subroutine')
        self.vm_writer.writeFunction(self.current_class + '.' + self.current_subroutine,self.subroutine_locals)
        if subroutineType == 'constructor':
            self.vm_writer.writePush('constant',self.symbol_table.varCount('field'))
            self.vm_writer.writeCall('Memory.alloc',1)
            self.vm_writer.writePop('pointer',0)
        if subroutineType == 'method':
            self.vm_writer.writePush('argument',0)
            self.vm_writer.writePop('pointer',0)
        if self.jt.getTokenValue() != '}':
            self.compileStatements()
        self.processTerminal()
        self.jt.nxt()
        
        self.writeXmlTag('subroutineBody','end')
        self.writeXmlTag('subroutineDec','end')
        
    def compileParameterList(self):
        self.writeXmlTag('parameterList','begin')
        if self.jt.getTokenValue() != ')':
            self.setIdentifierCategory('class')
            self.setIdentifierType(self.jt.getTokenValue())
            self.processTerminal()
            self.jt.nxt()
            
            self.setIdentifierCategory('argument')
            self.processTerminal()
            self.jt.nxt()
            
            while self.jt.getTokenValue() != ')':
                self.processTerminal()
                self.jt.nxt()
                
                self.setIdentifierCategory('class')
                self.processTerminal()
                self.jt.nxt()
                
                self.setIdentifierCategory('argument')
                self.processTerminal()
                self.jt.nxt()
        self.writeXmlTag('parameterList','end')
        
    def compileStatements(self):
        self.writeXmlTag('statements','begin')
        while self.jt.getTokenValue() != '}':
            if self.jt.getTokenValue() == 'let':
                self.compileLet()
            elif self.jt.getTokenValue() == 'if':
                self.compileIf()
            elif self.jt.getTokenValue() == 'while':
                self.compileWhile()
            elif self.jt.getTokenValue() == 'do':
                self.compileDo()
            elif self.jt.getTokenValue() == 'return':
                self.compileReturn()
        self.writeXmlTag('statements','end')
        
    def compileDo(self):
        self.writeXmlTag('doStatement','begin')
        self.processTerminal()
        self.jt.nxt()
        
        self.compileSubroutineCall()
        self.vm_writer.writePop('temp',0)
        self.processTerminal()
        self.jt.nxt()
        
        self.writeXmlTag('doStatement','end')
        
    def compileLet(self):
        self.writeXmlTag('letStatement','begin')
        self.processTerminal()
        self.jt.nxt()
        
        holdName = self.jt.getTokenValue()
        self.setIdentifierCategory(self.symbol_table.kindOf(holdName))
        self.processTerminal()
        self.jt.nxt()
        
        isArray = (self.jt.getTokenValue() == '[')
        if isArray:
            self.processTerminal()
            self.jt.nxt()
            
            self.compileExpression()
            self.processTerminal()
            self.jt.nxt()
            
            self.vm_writer.writePush(self.symbol_table.kindOf(holdName),self.symbol_table.indexOf(holdName))
            self.vm_writer.writeArithmetic('add')
            self.vm_writer.writePop('temp',1)
        self.processTerminal()
        self.jt.nxt()
        
        self.compileExpression()        
        self.processTerminal()
        self.jt.nxt()
        
        if isArray:
            self.vm_writer.writePush('temp',1)
            self.vm_writer.writePop('pointer',1)
            self.vm_writer.writePop('that',0)
        else:
            self.vm_writer.writePop(self.symbol_table.kindOf(holdName),self.symbol_table.indexOf(holdName))
        self.writeXmlTag('letStatement','end')
        
    def compileWhile(self):
        self.writeXmlTag('whileStatement','begin')
        L1 = self.current_class + self.current_subroutine + str(self.statement_label_tag)
        self.statement_label_tag += 1
        L2 = self.current_class + self.current_subroutine + str(self.statement_label_tag)
        self.statement_label_tag += 1
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeLabel(L1)
        self.processTerminal()
        self.jt.nxt()
        
        self.compileExpression()
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeArithmetic('not')
        self.vm_writer.writeIf(L2)
        self.processTerminal()
        self.jt.nxt()
        
        self.compileStatements()
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeGoTo(L1)
        self.vm_writer.writeLabel(L2)
        self.writeXmlTag('whileStatement','end')
        
    def compileReturn(self):
        self.writeXmlTag('returnStatement','begin')
        self.processTerminal()
        self.jt.nxt()
        
        if self.jt.getTokenValue() != ';':
            self.compileExpression()
        self.processTerminal()
        self.jt.nxt()
        
        if self.void_return:
            self.vm_writer.writePush('constant',0)
        self.vm_writer.writeReturn()
        self.writeXmlTag('returnStatement','end')
        
    def compileIf(self):
        self.writeXmlTag('ifStatement','begin')
        L1 = self.current_class + self.current_subroutine + str(self.statement_label_tag)
        self.statement_label_tag += 1
        L2 = self.current_class + self.current_subroutine + str(self.statement_label_tag)
        self.statement_label_tag += 1
        self.processTerminal()
        self.jt.nxt()
        
        self.processTerminal()
        self.jt.nxt()
        
        self.compileExpression()
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeArithmetic('not')
        self.vm_writer.writeIf(L1)
        self.processTerminal()
        self.jt.nxt()
        
        self.compileStatements()
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeGoTo(L2)
        self.vm_writer.writeLabel(L1)
        if self.jt.getTokenValue() == 'else':
            self.processTerminal()
            self.jt.nxt()
            
            self.processTerminal()
            self.jt.nxt()
            
            self.compileStatements()
            self.processTerminal()
            self.jt.nxt()
        self.vm_writer.writeLabel(L2)
        self.writeXmlTag('ifStatement','end')
        
    def compileExpression(self):
        self.writeXmlTag('expression','begin')
        self.setIdentifierCategory('var')
        self.compileTerm()
        while self.jt.getTokenValue() in ('+','-','*','/','&','|','<','>','='):
            op_postfix = self.jt.getTokenValue()
            self.processTerminal()
            self.jt.nxt()
            
            self.compileTerm()
            if op_postfix == '*':
                self.vm_writer.writeCall('Math.multiply',2)
            elif op_postfix == '/':
                self.vm_writer.writeCall('Math.divide',2)
            else:
                op_translate = {'+':'add','-':'sub','=':'eq','>':'gt','<':'lt','&':'and','|':'or'}
                self.vm_writer.writeArithmetic(op_translate[op_postfix])
        self.writeXmlTag('expression','end')
        
    def compileExpressionList(self):
        self.writeXmlTag('expressionList','begin')
        if self.jt.getTokenValue() != ')':
            self.compileExpression()
            self.subroutine_args += 1
            while self.jt.getTokenValue() != ')':
                self.processTerminal()
                self.jt.nxt()
                
                self.compileExpression()
                self.subroutine_args += 1
        self.writeXmlTag('expressionList','end')

    def compileSubroutineCall(self,subroutineName = None):
        self.subroutine_args = 0
        subroutine_call_name = ''
        if subroutineName:
            holdName = subroutineName
        else:
            holdName = self.jt.getTokenValue()
            self.jt.nxt()
        if self.jt.getTokenValue() == '.':
            if (holdName in self.symbol_table.subroutine_table) or (holdName in self.symbol_table.class_table):
                self.writeXmlTerminal('identifier',str(holdName))
                self.setIdentifierCategory(self.symbol_table.kindOf(holdName))
                self.vm_writer.writePush(self.symbol_table.kindOf(holdName),self.symbol_table.indexOf(holdName))
                self.subroutine_args += 1
                self.processTerminal()    
                self.jt.nxt()
                
                subroutine_call_name = self.symbol_table.typeOf(holdName) + '.' + self.jt.getTokenValue()
            else:
                self.writeXmlTerminal('identifier',str(holdName))
                self.setIdentifierCategory('class')
                self.processTerminal()    
                self.jt.nxt()
                
                subroutine_call_name = holdName + '.' + self.jt.getTokenValue()
            self.setIdentifierCategory('subroutine')
            self.processTerminal()
            self.jt.nxt()
        else:
            self.writeXmlTerminal('identifier',str(holdName))
            self.setIdentifierCategory('subroutine')
            self.vm_writer.writePush('pointer',0)
            self.subroutine_args += 1
            subroutine_call_name = self.current_class + '.' + holdName
        self.processTerminal()
        self.jt.nxt()
        
        self.compileExpressionList()
        self.processTerminal()
        self.jt.nxt()
        
        self.vm_writer.writeCall(subroutine_call_name,self.subroutine_args)
        
    def compileTerm(self):
        self.writeXmlTag('term','begin')
        if self.jt.getTokenType() == 'IDENTIFIER':
            holdName = self.jt.getTokenValue()
            self.jt.nxt()
            
            if self.jt.getTokenValue() in ('(','.'):
                self.compileSubroutineCall(holdName)
            elif self.jt.getTokenValue() == '[':
                self.writeXmlTerminal('identifier',str(holdName))
                self.setIdentifierCategory(self.symbol_table.kindOf(holdName))
                self.processTerminal()
                self.jt.nxt()
                
                self.compileExpression()
                self.processTerminal()
                self.jt.nxt()
                
                self.vm_writer.writePush(self.symbol_table.kindOf(holdName),self.symbol_table.indexOf(holdName))
                self.vm_writer.writeArithmetic('add')
                self.vm_writer.writePop('pointer',1)
                self.vm_writer.writePush('that',0)
            else:
                self.writeXmlTerminal('identifier',str(holdName))
                self.setIdentifierCategory(self.symbol_table.kindOf(holdName))
                self.vm_writer.writePush(self.symbol_table.kindOf(holdName),self.symbol_table.indexOf(holdName))
        elif self.jt.getTokenType() == 'SYMBOL':
            if self.jt.getTokenValue() == '-':
                self.processTerminal()
                self.jt.nxt()
                
                self.compileTerm()
                self.vm_writer.writeArithmetic('neg')
            elif self.jt.getTokenValue() == '~':
                self.processTerminal()
                self.jt.nxt()
                
                self.compileTerm()
                self.vm_writer.writeArithmetic('not')
            elif self.jt.getTokenValue() == '(':
                self.processTerminal()
                self.jt.nxt()
                
                self.compileExpression()
                self.processTerminal()
                self.jt.nxt()
        elif self.jt.getTokenType() == 'INT_CONST':
            self.vm_writer.writePush('constant',self.jt.getTokenValue())
            self.processTerminal()
            self.jt.nxt()
        elif self.jt.getTokenType() == 'KEYWORD':
            if self.jt.getTokenValue() in ('null','false'):
                self.vm_writer.writePush('constant',0)
            elif self.jt.getTokenValue() == 'true':
                self.vm_writer.writePush('constant',1)
                self.vm_writer.writeArithmetic('neg')
            elif self.jt.getTokenValue() == 'this':
                self.vm_writer.writePush('pointer',0)
            self.processTerminal()
            self.jt.nxt()
        elif self.jt.getTokenType() == 'STRING_CONST':
            s = self.jt.getTokenValue()
            maxLength = len(s)
            self.vm_writer.writePush('constant',maxLength)
            self.vm_writer.writeCall('String.new',1)
            for j in range(0,maxLength):
                self.vm_writer.writePush('constant',ord(s[j]))
                self.vm_writer.writeCall('String.appendChar',2)
            self.processTerminal()
            self.jt.nxt()
        else:
            self.processTerminal()
            self.jt.nxt()
        self.writeXmlTag('term','end')
        
class SymbolTable:
    def __init__(self):
        self.class_table = {}
        self.subroutine_table = {}
        self.var_count = {'static':0,'field':0,'argument':0,'var':0}
        
    def startSubroutine(self):
        self.subroutine_table = {}
        self.var_count['argument']=0
        self.var_count['var']=0
        
    def define(self,name,type,kind):
        if kind in ('static','field'):
            self.class_table[name] = (type,kind,self.varCount(kind))
            self.var_count[kind] += 1
        elif kind in ('argument','var'):
            self.subroutine_table[name] = (type,kind,self.varCount(kind))
            self.var_count[kind] += 1
        
    def varCount(self,kind):
        if kind in self.var_count:
            return self.var_count[kind]
        
    def kindOf(self,name):
        if name in self.subroutine_table:
            return self.subroutine_table[name][1]
        elif name in self.class_table:
            return self.class_table[name][1]
        
    def typeOf(self,name):
        if name in self.subroutine_table:
            return self.subroutine_table[name][0]
        elif name in self.class_table:
            return self.class_table[name][0]
        
    def indexOf(self,name):
        if name in self.subroutine_table:
            return self.subroutine_table[name][2]
        elif name in self.class_table:
            return self.class_table[name][2]