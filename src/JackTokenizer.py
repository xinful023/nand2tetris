#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fu
"""

import re,os

class JackTokenizer:
    def __init__(self,jack_filename):
        self.keywords = {'class','constructor','function','method','field','static','var','int','char','boolean','void','true','false','null','this','let','do','if','else','while','return'}
        self.symbols  = {'{','}','(',')','[',']','.',',',';','+','-','*','/','&','|','<','>','=','~'}
        self.token_specification = [('IDENTIFIER',r'[a-zA-Z_][A-Za-z_\d]*'),('INT_CONST',r'\d+'),('STRING_CONST',r'\".+\"'),('OTHER',r'[^\s]')]
        self.tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in self.token_specification)
        self.xml_translate = {'KEYWORD':'keyword','SYMBOL':'symbol','INT_CONST':'integerConstant','STRING_CONST':'stringConstant','IDENTIFIER':'identifier'}
        
        self.jack_filename = jack_filename
        self.jack = open(jack_filename,'r')
        self.xmlT_filename = os.path.splitext(jack_filename)[0] + 'T.xml'
        self.xmlT = open(self.xmlT_filename,'w')
        self.xmlT.write('<tokens>' + '\n')
        self.readEOF = False
        self.token_list = []
        self.token_list_temp = []
        self.token_list_cur = 0
        self.nextLine()
        
    def __del__(self):
        self.xmlT.write('</tokens>')
        self.xmlT.close()
        
    def getTokenType(self):
        return self.token_list[self.token_list_cur][0]
        
    def getTokenValue(self):
        return self.token_list[self.token_list_cur][1]
        
    def nxt(self):
        if not self.readEOF:
            self.token_list_cur += 1
            if (self.token_list_cur >= len(self.token_list)):
                self.nextLine()
                
    def nextLine(self):
        self.line = self.jack.readline()
        if(self.line == ''):
            self.readEOF = True
            self.token_list_cur -= 1
        else:
            self.tokenizeLine()
            self.token_list = self.token_list_temp[:]
            self.token_list_cur = 0    
       
    def tokenizeLine(self):
        self.token_list_temp.clear()
        for mo in re.finditer(self.tok_regex,self.line):
            tokenType = mo.lastgroup
            value = mo.group(tokenType)
            if tokenType == 'IDENTIFIER' and value in self.keywords:
                tokenType = 'KEYWORD'
            elif tokenType == 'STRING_CONST':
                value = value[1:-1]
            elif tokenType == 'OTHER':
                if value in self.symbols:
                    tokenType = 'SYMBOL'
            self.writeTokenXml(tokenType,value)        
            self.token_list_temp.append((tokenType,value))
        
    def writeTokenXml(self,tokenType,value):
        xml_value = str(value)
        if xml_value == '<':
            xml_value = '&lt;'
        if xml_value == '>':
            xml_value = '&gt;'
        if xml_value == '&':
            xml_value = '&amp;'
                
        xml = '\t<' + self.xml_translate[tokenType] + '> ' + xml_value + ' </' + self.xml_translate[tokenType] + '>\n'
        self.xmlT.write(xml)