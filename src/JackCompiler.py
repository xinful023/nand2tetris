#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fu
"""

import sys,os,glob

from JackTokenizer import JackTokenizer
from CompilationEngine import CompilationEngine

inpt = sys.argv[-1]
if inpt.endswith(".jack"):
    i = [inpt]
elif inpt.endswith('/'):
    i = glob.glob(inpt + "*.jack")
    
for f in i:
    cmd = "python3 rmWhiteSpace.py no-comments " + f
    os.system(cmd)
    filename = os.path.splitext(f)[0] + '.out'
    jt = JackTokenizer(filename)
    ce = CompilationEngine(jt)